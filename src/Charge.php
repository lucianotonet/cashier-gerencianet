<?php


/**
{
  "code": 200,
  "data": {
    "charge_id": 67477,
    "total": 50000,
    "status": "new",
    "subscription": {
      "subscription_id": 2099,
      "status": "canceled",
      "plan_id": 1759
    },
    "custom_id": null,
    "created_at": "2016-06-20 00:31:07",
    "notification_url": null,
    "items": [
      {
        "name": "Item 1",
        "value": 1000,
        "amount": 10
      },
      {
        "name": "Item 2",
        "value": 2000,
        "amount": 20
      }
    ],
    "history": [
      {
        "message": "Cobrança criada",
        "created_at": "2016-06-20 00:31:07"
      }
    ]
  }
}
 */

namespace Laravel\Cashier;

use Exception;
use Carbon\Carbon;
use LogicException;
use InvalidArgumentException;
use Illuminate\Database\Eloquent\Model;
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

class Charge extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];

    /**
     * Get the user that owns the charge.
     */
    public function user()
    {
        $model = getenv('GERENCIANET_MODEL') ?: config('services.gerencianet.model');

        return $this->belongsTo($model, 'user_id');
    }

    /**
     * Determine if the charge is active, on trial, or within its grace period.
     *
     * @return bool
     */
    public function valid()
    {
        return $this->active();
    }

    /**
     * Determine if the charge is active.
     *
     * @return bool
     */
    public function active()
    {
        return ! $this->cancelled();
    }

    /**
     * Determine if the charge is no longer active.
     *
     * @return bool
     */
    public function cancelled()
    {        
        // print_r($this->status);
        return $this->status == "cancelled";    
        // return ! is_null($this->ends_at);
    }
    

    /**
     * Cancel the charge.
     *
     * @return $this
     */
    public function cancel()
    {        
        $api = new GerencianetChargeService;
        $charge = $api->cancel( $this->charge_id );

        //  If Cancelled succefuly
        if( $charge['code'] == 200 ){ 	
            $this->status = 'cancelled';
            $this->save();
        }

        return $this;
    }



}
