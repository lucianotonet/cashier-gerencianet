<?php

//     "getPlans": {
//       "route": "/v1/plans",
//       "method": "get"
//     },
//     "createPlan": {
//       "route": "/v1/plan",
//       "method": "post"
//     },
//     "deletePlan": {
//       "route": "/v1/plan/:id",
//       "method": "delete"
//     },
//     "createSubscription": {
//       "route": "/v1/plan/:id/subscription",
//       "method": "post"
//     },
//     "detailSubscription": {
//       "route": "/v1/subscription/:id",
//       "method": "get"
//     },
//     "paySubscription": {
//       "route": "/v1/subscription/:id/pay",
//       "method": "post"
//     },
//     "cancelSubscription": {
//       "route": "/v1/subscription/:id/cancel",
//       "method": "put"
//     },
//     "updateSubscriptionMetadata": {
//       "route": "/v1/subscription/:id/metadata",
//       "method": "put"
//     },

namespace Laravel\Cashier;

use Exception;
// use Gerencianet\Plan as GerencianetPlan;

use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

class GerencianetSubscriptionService
{	
	protected $options;
	
	protected $api;

	public function __construct()
	{
		$this->options = [
	        'client_id'       => getenv('GERENCIANET_CLIENT_ID'),
	        'client_secret'   => getenv('GERENCIANET_CLIENT_SECRET'),
	        'sandbox'         => true
	    ]; 

	    $this->api = new Gerencianet( $this->options );
	}

	/**
	 * Create a plan
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function createPlan($params)
	{		
		$plan = $this->api->createPlan([], $params);
		return $plan;
		
	}

	/**
	 * Delete a plan
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function deletePlan($id)
	{
		$params = ['id' => $id];
				
		$plan = $this->api->deletePlan($params, []);
		print_r( $plan );
		return $plan;

	}

	/**
	 * List all the Gerencianet plans
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function getPlans($limit = 20, $offset = 0)
	{
		$params = [$limit, $offset];
	    
	    $plans = $this->api->getPlans($params, []);

	    return $plans;
	}

    /**
     * Get the Gerencianet plan that has the given ID or Name.
     *
     * @param  string  $id
     * @return \Gerencianet\Plan
     */
    public function findPlan($string)
    {    	
    	$plans = $this->getPlans();  

    	$string = (Array)$string;  	    	

    	foreach ($plans['data'] as $plan) {    		
    		
    		if( is_array($string) && in_array($plan['name'], $string) || in_array($plan['plan_id'], $string) ){
    			$reponse[] = $plan;
    		} 

			if( $plan['name'] === $string || $plan['plan_id'] === intval($string) ){
    			return $plan;
    		} 
    	}   

    	if( @$reponse ){
    		return ( count($reponse) == 1) ? $reponse[0] : $reponse;
    	}

    	return false;

    }


    // ---------------------------------------
    public function createSubscription($plan_id, $items, $user, $name)
    {
  		$params = ['id' => $plan_id ];
		
		$body = [
			'items' => $items
		];
	    
	    $subscription 	= $this->api->createSubscription($params, $body);

		$subscription 	= $subscription['data'];

		if( ! $subscription ){
			return false;
		}

		$newSubscription = new Subscription;

		if ($newSubscription->skipTrial) {
            $trialEndsAt = null;
        } else {
            $trialEndsAt = $newSubscription->trialDays ? Carbon::now()->addDays($newSubscription->trialDays) : null;
        }

		return $newSubscription->create([
								    	"name" 				=> $name,
								    	"user_id"        	=> $user->id,
									    "subscription_id" 	=> $subscription['subscription_id'],    
									    "status" 			=> "new",
									    "custom_id" 		=> null,
									    "notification_url" 	=> null,
									    "payment_method"	=> null,
									    "next_execution"	=> null,
									    "next_expire_at" 	=> null,    
									    "plan_id" 			=> $plan_id,    
									    "occurrences" 		=> 0,
									    // local
									    'trial_ends_at' => $trialEndsAt,
			        					'ends_at' 		=> null,
			        				]);		

    }


    public function cancelSubscription( $id )
    {
    	$params = ['id' => $id];
	    return $this->api->cancelSubscription($params, []);	    
    }

    public function detailSubscription( $id )
    {
   		$params = ['id' => intval( $id ) ];
    	return $this->api->detailSubscription($params, []);
    }

}