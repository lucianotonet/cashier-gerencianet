<?php

namespace Laravel\Cashier;

use Exception;
use Carbon\Carbon;
use Illuminate\Support\Arr;
// use Gerencianet\PaymentMethod;
// use Gerencianet\PaypalAccount;
use InvalidArgumentException;
// use Gerencianet\TransactionSearch;
use Illuminate\Support\Collection;
// use Gerencianet\Customer as GerencianetCustomer;
// use Gerencianet\Transaction as GerencianetTransaction;
// use Gerencianet\Subscription as GerencianetSubscription;

use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

        
trait Billable
{

    /**
     * Make a "one off" charge on the customer for the given amount.
     *
     * @param  int  $amount
     * @param  array  $options
     * @return \Gerencianet\Charge
     *
     * @throws \Gerencianet\Error\Card
     */
    public function charge($items = [])
    {        
        $api  = new GerencianetChargeService;
        $charge = $api->create($this, $items);



        return $charge;
    }

    public function charges()
    {
        return $this->hasMany(Charge::class, 'user_id')->orderBy('created_at', 'desc');
        
        // $api  = new GerencianetChargeService;
        // $localCharges = $this->hasMany(Charge::class, 'user_id');

        // foreach ($localCharges as $charge) {
        //     $charges[] = $api->detail($charge['id']);
        // }

        // return $charges;
    }

    /**
     * Refund a customer for a charge.
     *
     * @param  string  $charge
     * @param  array  $options
     * @return \Gerencianet\Charge
     *
     * @throws \Gerencianet\Error\Refund
     */
    public function refund($charge, array $options = [])
    {
        $options['charge'] = $charge;

        return GerencianetRefund::create($options, ['api_key' => $this->getGerencianetKey()]);
    }

    /**
     * Determines if the customer currently has a card on file.
     *
     * @return bool
     */
    public function hasCardOnFile()
    {
        return (bool) $this->card_brand;
    }

    /**
     * Invoice the customer for the given amount.
     *
     * @param  string  $description
     * @param  int  $amount
     * @param  array  $options
     * @return bool
     *
     * @throws \Gerencianet\Error\Card
     */
    public function invoiceFor($description, $amount, array $options = [])
    {
        if (! $this->gerencianet_id) {
            throw new InvalidArgumentException('User is not a customer. See the createAsGerencianetCustomer method.');
        }

        $options = array_merge([
            'customer' => $this->gerencianet_id,
            'amount' => $amount,
            'currency' => $this->preferredCurrency(),
            'description' => $description,
        ], $options);

        GerencianetInvoiceItem::create(
            $options, ['api_key' => $this->getGerencianetKey()]
        );

        return $this->invoice();
    }

    /**
     * Begin creating a new subscription.
     *
     * @param  string  $subscription
     * @param  string  $plan
     * @return \Laravel\Cashier\SubscriptionBuilder
     */
    public function newSubscription($subscription, $plan_id)
    {        
        $items = [
            [
                'name' => 'Item 1',
                'amount' => 10,
                'value' => 1000
            ],
              [
                'name' => 'Item 2',
                'amount' => 20,
                'value' => 2000
              ]
        ];
        
        $api  = new GerencianetSubscriptionService;
        $plan = $api->findPlan( $plan_id );        

        $subscription = $api->createSubscription( $plan['plan_id'], $items, $this, $subscription);

        return $subscription;
    }

    /**
     * Determine if the user is on trial.
     *
     * @param  string  $subscription
     * @param  string|null  $plan
     * @return bool
     */
    public function onTrial($subscription = 'default', $plan = null)
    {
        if (func_num_args() === 0 && $this->onGenericTrial()) {
            return true;
        }

        $subscription = $this->subscription($subscription);

        if (is_null($plan)) {
            return $subscription && $subscription->onTrial();
        }

        return $subscription && $subscription->onTrial() &&
               $subscription->plan_id === $plan;
    }

    /**
     * Determine if the user is on a "generic" trial at the user level.
     *
     * @return bool
     */
    public function onGenericTrial()
    {
        return $this->trial_ends_at && Carbon::now()->lt($this->trial_ends_at);
    }

    /**
     * Determine if the user has a given subscription.
     *
     * @param  string  $subscription
     * @param  string|null  $plan
     * @return bool
     */
    public function subscribed($subscription = 'default', $plan = null)
    {
        $subscription = $this->subscription($subscription);        

        if (is_null($subscription)) {
            return false;
        }

        if (is_null($plan)) {
            return $subscription->valid();
        }

        $api  = new GerencianetSubscriptionService;
        $plan = $api->findPlan($plan);

        return $subscription->valid() && $subscription->plan_id == $plan['plan_id'];
    }

    /**
     * Get a subscription instance by name.
     *
     * @param  string  $subscription
     * @return \Laravel\Cashier\Subscription|null
     */
    public function subscription($subscription = 'main')
    {
        return $this->subscriptions->sortByDesc(function ($value) {
            return $value->created_at->getTimestamp();
        })
        ->first(function ($key, $value) use ($subscription) {
            return $value->name === $subscription;
        });
    }

    /**
     * Get all of the subscriptions for the user.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'user_id')->orderBy('created_at', 'desc');
    }

    /**
     * Invoice the billable entity outside of regular billing cycle.
     *
     * @return GerencianetInvoice|bool
     */
    public function invoice()
    {
        if ($this->gerencianet_id) {
            try {
                return GerencianetInvoice::create(['customer' => $this->gerencianet_id], $this->getGerencianetKey())->pay();
            } catch (GerencianetErrorInvalidRequest $e) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the entity's upcoming invoice.
     *
     * @return \Laravel\Cashier\Invoice|null
     */
    public function upcomingInvoice()
    {
        try {
            $gerencianetInvoice = GerencianetInvoice::upcoming(
                ['customer' => $this->gerencianet_id], ['api_key' => $this->getGerencianetKey()]
            );

            return new Invoice($this, $gerencianetInvoice);
        } catch (GerencianetErrorInvalidRequest $e) {
            //
        }
    }

    /**
     * Find an invoice by ID.
     *
     * @param  string  $id
     * @return \Laravel\Cashier\Invoice|null
     */
    public function findInvoice($id)
    {
        try {
            return new Invoice($this, GerencianetInvoice::retrieve($id, $this->getGerencianetKey()));
        } catch (Exception $e) {
            //
        }
    }

    /**
     * Find an invoice or throw a 404 error.
     *
     * @param  string  $id
     * @return \Laravel\Cashier\Invoice
     */
    public function findInvoiceOrFail($id)
    {
        $invoice = $this->findInvoice($id);

        if (is_null($invoice)) {
            throw new NotFoundHttpException;
        }

        return $invoice;
    }

    /**
     * Create an invoice download Response.
     *
     * @param  string  $id
     * @param  array   $data
     * @param  string  $storagePath
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function downloadInvoice($id, array $data, $storagePath = null)
    {
        return $this->findInvoiceOrFail($id)->download($data, $storagePath);
    }

    /**
     * Get a collection of the entity's invoices.
     *
     * @param  bool  $includePending
     * @param  array  $parameters
     * @return \Illuminate\Support\Collection
     */
    public function invoices($includePending = false, $parameters = [])
    {
        return $this->hasMany( Invoice::class, 'user_id' )->orderBy('created_at', 'desc');        
    }

    /**
     * Get an array of the entity's invoices.
     *
     * @param  array  $parameters
     * @return \Illuminate\Support\Collection
     */
    public function invoicesIncludingPending(array $parameters = [])
    {
        return $this->invoices(true, $parameters);
    }

    /**
     * Update customer's credit card.
     *
     * @param  string  $token
     * @return void
     */
    public function updateCard($token)
    {
        $customer = $this->asGerencianetCustomer();

        $token = GerencianetToken::retrieve($token, ['api_key' => $this->getGerencianetKey()]);

        // If the given token already has the card as their default source, we can just
        // bail out of the method now. We don't need to keep adding the same card to
        // the user's account each time we go through this particular method call.
        if ($token->card->id === $customer->default_source) {
            return;
        }

        $card = $customer->sources->create(['source' => $token]);

        $customer->default_source = $card->id;

        $customer->save();

        // Next, we will get the default source for this user so we can update the last
        // four digits and the card brand on this user record in the database, which
        // is convenient when displaying on the front-end when updating the cards.
        $source = $customer->default_source
                    ? $customer->sources->retrieve($customer->default_source)
                    : null;

        $this->fillCardDetails($source);

        $this->save();
    }

    /**
     * Synchronises the customer's card from Gerencianet back into the database.
     *
     * @return $this
     */
    public function updateCardFromGerencianet()
    {
        $customer = $this->asGerencianetCustomer();

        $defaultCard = null;

        foreach ($customer->sources->data as $card) {
            if ($card->id === $customer->default_source) {
                $defaultCard = $card;
                break;
            }
        }

        if ($defaultCard) {
            $this->fillCardDetails($defaultCard)->save();
        } else {
            $this->forceFill([
                'card_brand' => null,
                'card_last_four' => null,
            ])->save();
        }

        return $this;
    }

    /**
     * Fills the user's properties with the source from Gerencianet.
     *
     * @param \Gerencianet\Card|null  $card
     * @return $this
     */
    protected function fillCardDetails($card)
    {
        if ($card) {
            $this->card_brand = $card->brand;
            $this->card_last_four = $card->last4;
        }

        return $this;
    }


    /**
     * Determine if the user is actively subscribed to one of the given plans.
     *
     * @param  array|string  $string
     * @param  string  $subscription
     * @return bool
     */
    public function subscribedToPlan($string, $subscription = 'default')
    {
        $subscription = $this->subscription($subscription);

        if (! $subscription || ! $subscription->valid()) {
            return false;
        }

        $api                = new GerencianetSubscriptionService;
        $subscriptionPlan   = $api->findPlan( $subscription->plan_id ); 

        foreach ((array) $string as $search) {
            if ( $subscriptionPlan['name'] == $search || $subscriptionPlan['plan_id'] === $search ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if the entity is on the given plan.
     *
     * @param  string  $plan
     * @return bool
     */
    public function onPlan($plan)
    {
        return ! is_null($this->subscriptions->first(function ($key, $value) use ($plan) {
            return $value->plan_id === $plan && $value->valid();
        }));
    }

    /**
     * Determine if the entity has a Gerencianet customer ID.
     *
     * @return bool
     */
    public function hasGerencianetId()
    {
        return ! is_null($this->gerencianet_id);
    }

    /**
     * Create a Gerencianet customer for the given user.
     *
     * @param  string  $token
     * @param  array  $options
     * @return GerencianetCustomer
     */
    public function createAsGerencianetCustomer($token, array $options = [])
    {
        $options = array_key_exists('email', $options)
                ? $options : array_merge($options, ['email' => $this->email]);

        // Here we will create the customer instance on Gerencianet and store the ID of the
        // user from Gerencianet. This ID will correspond with the Gerencianet user instances
        // and allow us to retrieve users from Gerencianet later when we need to work.
        $customer = GerencianetCustomer::create(
            $options, $this->getGerencianetKey()
        );

        $this->gerencianet_id = $customer->id;

        $this->save();

        // Next we will add the credit card to the user's account on Gerencianet using this
        // token that was provided to this method. This will allow us to bill users
        // when they subscribe to plans or we need to do one-off charges on them.
        if (! is_null($token)) {
            $this->updateCard($token);
        }

        return $customer;
    }

    /**
     * Get the Gerencianet customer for the user.
     *
     * @return \Gerencianet\Customer
     */
    public function asGerencianetCustomer()
    {
        return GerencianetCustomer::retrieve($this->gerencianet_id, $this->getGerencianetKey());
    }

    /**
     * Get the Gerencianet supported currency used by the entity.
     *
     * @return string
     */
    public function preferredCurrency()
    {
        return Cashier::usesCurrency();
    }

    /**
     * Get the tax percentage to apply to the subscription.
     *
     * @return int
     */
    public function taxPercentage()
    {
        return 0;
    }

    /**
     * Get the Gerencianet API key.
     *
     * @return string
     */
    public static function getGerencianetKey()
    {
        return static::$gerencianetClientId ?: getenv('GERENCIANET_CLIENT_ID');
    }

    /**
     * Set the Gerencianet API key.
     *
     * @param  string  $key
     * @return void
     */
    public static function setGerencianetKey($key)
    {
        static::$gerencianetClientId = $key;
    }
}
