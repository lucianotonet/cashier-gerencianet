<?php

use Carbon\Carbon;
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Http\Request;
use Laravel\Cashier\GerencianetSubscriptionService;
use Laravel\Cashier\GerencianetChargeService;
use Laravel\Cashier\Http\Controllers\WebhookController;

class CashierTest extends PHPUnit_Framework_TestCase
{

    private $options;
    private $plans;

    private $user;

    public static function setUpBeforeClass()
    {
        if (file_exists(__DIR__.'/../.env')) {
            $dotenv = new Dotenv\Dotenv(__DIR__.'/../');
            $dotenv->load();
        }      
    }

    public function setUp()
    {

        // Define the Gerencianet KEYS
        $this->options = [
            'client_id'       => getenv('GERENCIANET_CLIENT_ID'),
            'client_secret'   => getenv('GERENCIANET_CLIENT_SECRET'),
            'sandbox'         => true
        ];                    

        Eloquent::unguard();

        $db = new DB;
        $db->addConnection([
            'driver' => 'sqlite',
            'database' => ':memory:',
            ]);
        $db->bootEloquent();
        $db->setAsGlobal();

        // Cria tabela users
        $this->schema()->create('users', function ($table) {
            $table->increments('id');
            $table->string('email');
            $table->string('name');
            $table->string('gerencianet_id')->nullable();
            $table->string('card_brand')->nullable();
            $table->string('card_last_four')->nullable();
            $table->timestamps();
        });

        // Cria tabela subscriptions
        $this->schema()->create('subscriptions', function ($table) {

            $table->increments('id');

            $table->string('name')->nullable();
            $table->integer('user_id');  
            $table->integer('subscription_id');     // 1876
            $table->string('status')->default("new");
            $table->string('custom_id')->nullable();
            $table->string('notification_url')->nullable();
            $table->string('payment_method')->nullable();
            $table->datetime('next_execution')->nullable();
            $table->datetime('next_expire_at')->nullable();
            $table->integer('plan_id')->nullable();
            $table->integer('occurrences')->default(0);

            // LOCAL TRIAL MANAGMENT
            $table->datetime('trial_ends_at')->nullable();
            $table->datetime('ends_at')->nullable();

            $table->timestamps();  

        });


        // Cria tabela transactions
        $this->schema()->create('charges', function ($table) {

            $table->increments('id');

            $table->integer('charge_id');     // 67477
            $table->integer('user_id');  
            $table->integer('total');  
            $table->string('status')->default("new");
            $table->string('custom_id')->nullable();
            $table->string('notification_url')->nullable();
           
            $table->timestamps();  

        });


        $faker = Faker\Factory::create();        

        $user = User::create([
                'email' => $faker->email,
                'name'  => $faker->name,
            ]);        
        $this->user = $user;

        
        $api = new GerencianetSubscriptionService;

        // Create Plans      
        $plansToCreate = [
            [
                'name' => 'mensal',
                'interval' => 1,
                 // 'repeats' => NULL 
            ],
            [
                'name' => 'trimestral',
                'interval' => 3,
                 // 'repeats' => NULL 
            ],
            [
                'name' => 'semestral',
                'interval' => 6,
                 // 'repeats' => NULL 
            ],
            [
                'name' => 'anual',
                'interval' => 12,
                 // 'repeats' => NULL 
            ]
        ];  
    
        foreach ($plansToCreate as $params) {
            $api->createPlan($params);
        }           

        $plans  = $api->getPlans(20,0);        
        
        // Save plans for tests
        $this->plans = $plans['data'];

    }


    public function tearDown()
    {
        $api = new GerencianetSubscriptionService;

        // Delete all Plans
        $plans  = $this->plans;     

        // foreach ($plans as $plan) {   
        //     $api->deletePlan( $plan['plan_id'] );                    
        // }        

        $this->schema()->drop('users');
        $this->schema()->drop('subscriptions');
        $this->schema()->drop('charges');

    }


    /**
     *  Tests...
     */
    public function testPlans()
    {        
        $api   = new GerencianetSubscriptionService;        

        $plans  = $api->getPlans();
        $this->assertContains( $this->plans, $plans );       

        $plan  = $api->findPlan('mensal');
        $this->assertEquals('mensal', $plan['name'] );

        $plans  = $api->findPlan(['mensal','semestral']);
        $this->assertEquals(2, count( $plans ) );

        $plans  = $api->findPlan( array_column( $this->plans, 'name' ) );
        $this->assertEquals( count( $plans ), count( $this->plans ) );    
    }

    
    public function testSubscriptions()
    {
        $user = $this->user;
        
        $api   = new GerencianetSubscriptionService;
        $plan  = $api->findPlan('mensal');

        # https://laravel.com/docs/5.2/billing#subscriptions
        $subscription = $user->newSubscription( 'main', $plan['plan_id'] );        

        $this->assertEquals(1, count($user->subscriptions));
        $this->assertNotNull( $user->subscription('main')->subscription_id );
        $this->assertTrue( $user->subscribed( 'main' ) );
        $this->assertTrue( $user->subscribedToPlan( $plan['plan_id'], 'main' ) );
        $this->assertFalse( $user->subscribedToPlan( $plan['plan_id'], 'something' ) );
        $this->assertFalse( $user->subscribedToPlan('monthly-10-2', 'main') );

        $this->assertTrue($user->subscribed('main', 'mensal'));
        $this->assertFalse($user->subscribed('main', 'monthly-10-2'));

        $this->assertTrue($user->subscription( 'main' )->active());
        $this->assertFalse($user->subscription('main')->cancelled());
        $this->assertFalse($user->subscription('main')->onGracePeriod());

        // Cancel Subscription
        $subscription = $user->subscription('main');
        $subscription->cancel();
        
        $this->assertFalse($subscription->active());
        $this->assertTrue($subscription->cancelled());
        $this->assertFalse($subscription->onGracePeriod());

        // print_r( $subscription );
        // print_r( $subscription->status );
        // print_r( $user->subscriptions );

        // Modify Ends Date To Past
        // $oldGracePeriod = $subscription->ends_at;
        // $subscription->fill(['ends_at' => Carbon::now()->subDays(5)])->save();

        // $this->assertFalse($subscription->active());
        // $this->assertTrue($subscription->cancelled());
        // $this->assertFalse($subscription->onGracePeriod());

        // $subscription->fill(['ends_at' => $oldGracePeriod])->save();

        




        // Resume Subscription
        //  THE RESUME IS NOT POSSIBLE IN GERENCIANET  
        // $subscription->resume();

        // $this->assertTrue($subscription->active());
        // $this->assertFalse($subscription->cancelled());
        // $this->assertFalse($subscription->onGracePeriod());

        // // Increment & Decrement
        // $subscription->incrementQuantity();

        // $this->assertEquals(2, $subscription->quantity);

        // $subscription->decrementQuantity();

        // $this->assertEquals(1, $subscription->quantity);

        // // Swap Plan
        // $subscription->swap('monthly-10-2');

        // $this->assertEquals('monthly-10-2', $subscription->stripe_plan);

        // Invoice Tests
        // $invoice = $user->invoices()[1];

        // $this->assertEquals('$10.00', $invoice->total());
        // $this->assertFalse($invoice->hasDiscount());
        // $this->assertFalse($invoice->hasStartingBalance());
        // $this->assertNull($invoice->coupon());
        // $this->assertInstanceOf(Carbon::class, $invoice->date());

    }    

    public function testCharges()
    {
        $user = $this->user;

        $items = [
            [
                'name' => 'Site #001',
                'amount' => 1,
                'value' => 1490
            ],
            [
                'name' => 'Site #002',
                'amount' => 1,
                'value' => 1490
            ]
        ];

        // New Charge
        $charge = $user->charge( $items );

        $this->assertEquals('new', $charge['status']);
        $this->assertEquals(2980, $charge['total']);

        // Get user Charges
        $charges = $user->charges->toArray();
        $this->assertEquals(1, count($charges) );        

        // Get Charge Details
        $invoices = $user->invoices;
        print_r( $invoices );



    }
    

    

    protected function getTestToken()
    {
        return Stripe\Token::create([
            'card' => [
            'number' => '4242424242424242',
            'exp_month' => 5,
            'exp_year' => 2020,
            'cvc' => '123',
            ],
            ], ['api_key' => getenv('STRIPE_SECRET')])->id;
    }

    /**
     * Schema Helpers.
     */
    protected function schema()
    {
        return $this->connection()->getSchemaBuilder();
    }

    protected function connection()
    {
        return Eloquent::getConnectionResolver()->connection();
    }
}

class User extends Eloquent
{
    use Laravel\Cashier\Billable;
}

class CashierTestControllerStub extends WebhookController
{
    protected function eventExistsOnStripe($id)
    {
        return true;
    }
}
