<?php

/**
 * 
{
  "code": 200,
  "data": {
    "subscription_id": 2099,
    "value": 50000,
    "status": "canceled",
    "custom_id": null,
    "notification_url": null,
    "payment_method": null,
    "next_execution": null,
    "next_expire_at": null,
    "plan": {
      "plan_id": 1759,
      "name": "mensal",
      "interval": 1,
      "repeats": null
    },
    "occurrences": 0,
    "created_at": "2016-06-20 00:31:07",
    "history": [
      {
        "charge_id": 67477,
        "status": "new",
        "created_at": "2016-06-20 00:31:07"
      }
    ]
  }
}
 */

namespace Laravel\Cashier;

use Exception;
use Carbon\Carbon;
use LogicException;
use InvalidArgumentException;
use Illuminate\Database\Eloquent\Model;
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

class Subscription extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The number of trial days to apply to the subscription.
     *
     * @var int|null
     */
    protected $trialDays = 15;

    /**
     * Indicates that the trial should end immediately.
     *
     * @var bool
     */
    protected $skipTrial = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'trial_ends_at', 'ends_at',
        'created_at', 'updated_at',
    ];

    /**
     * Get the user that owns the subscription.
     */
    public function user()
    {
        $model = getenv('GERENCIANET_MODEL') ?: config('services.gerencianet.model');

        return $this->belongsTo($model, 'user_id');
    }

    /**
     * Determine if the subscription is active, on trial, or within its grace period.
     *
     * @return bool
     */
    public function valid()
    {
        return $this->active() || $this->onTrial() || $this->onGracePeriod();
    }

    /**
     * Determine if the subscription is active.
     *
     * @return bool
     */
    public function active()
    {
        return ! $this->cancelled() || $this->onGracePeriod();
    }

    /**
     * Determine if the subscription is no longer active.
     *
     * @return bool
     */
    public function cancelled()
    {        
        // print_r($this->status);
        return $this->status == "cancelled";    
        // return ! is_null($this->ends_at);
    }

    /**
     * Determine if the subscription is within its trial period.
     *
     * @return bool
     */
    public function onTrial()
    {
        if (! is_null($this->trial_ends_at)) {
            return Carbon::today()->lt($this->trial_ends_at);
        } else {
            return false;
        }
    }

    /**
     * Determine if the subscription is within its grace period after cancellation.
     *
     * @return bool
     */
    public function onGracePeriod()
    {
        if (! is_null($endsAt = $this->ends_at)) {
            return Carbon::now()->lt(Carbon::instance($endsAt));
        } else {
            return false;
        }
    }


    /**
     * Cancel the subscription.
     *
     * @return $this
     */
    public function cancel()
    {        
        $api = new GerencianetSubscriptionService;
        $subscription = $api->cancelSubscription( $this->subscription_id );

        if( $subscription['code'] == 200 ){ // Cancelled succefuly
            $this->status = 'cancelled';
            $this->save();
        }

        return $this;
    }



}
