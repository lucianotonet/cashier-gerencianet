<?php
namespace Laravel\Cashier;

use Exception;

use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

class GerencianetChargeService
{	
	protected $options;
	
	protected $api;

	public function __construct()
	{
		$this->options = [
	        'client_id'       => getenv('GERENCIANET_CLIENT_ID'),
	        'client_secret'   => getenv('GERENCIANET_CLIENT_SECRET'),
	        'sandbox'         => true
	    ]; 

	    $this->api = new Gerencianet( $this->options );
	}

	/**	 
	 * Billet (ticket)
	 * 
	 *  $params = ['id' => 0];
	 *
	 *	$customer = [
	 *		'name' => 'Gorbadoc Oldbuck',
	 *		'cpf' => '04267484171',
	 *		'phone_number' => '5144916523'
	 *	];
 	 *
 	 *	$body = [
 	 *	  'payment' => [
 	 *	    'banking_billet' => [
 	 *	      'expire_at' => '2018-12-12',
 	 *	      'customer' => $customer
 	 *	    ]
 	 *	  ]
 	 *	];
 	 * 
	 * @param  Int 		$id   	Charge ID
	 * @param  Array 	$body 	Customer and Payment data
	 * @return Array    	
	 * @category Services
  	 * @package  Cashier-Gerencianet
	 *  
	 */
	public function billet ( $id,  $body){	 
		$params = ['id' => $id];   		
	    return $this->api->payCharge($params, $body);
	}

	/**
	 * [cancel description]
	 * @return [type] [description]
	 */
	public function cancel (){

	}
	
	public function card (){

	}
	
	/**
	 * [create description]
	 * @param  array  $items [description]
	 * @return [type]        [description]
	 */
	public function create( $user, $items = [] ){
		$body = [
          'items' => $items
        ];	    
	    $charge = $this->api->createCharge([], $body);

	    if( $charge['code'] == 200 ){

	    	$charge['data']['user_id'] = $user->id;
	    	$transaction = Charge::create( $charge['data'] );	    	

	    	return $transaction;
	    }

	    return $charge;
	}
	
	public function createChargeHistory (){

	}
	
	public function detail( $id = null){
		$params = ['id' => $id];
		return $this->api->detailCharge($params, []);
	}
	
	public function resendBillet (){

	}
	
	public function shipping (){

	}
	
	public function updateBillet (){

	}
	
	public function updateMetadata (){

	}

}