<?php

// require __DIR__.'../vendor/autoload.php';

use Sami\Sami;
use Symfony\Component\Finder\Finder;
use Sami\Version\GitVersionCollection;
use Sami\RemoteRepository\BitBucketRemoteRepository;

$iterator = Finder::create()
	->files()
	->name('*.php')
	->exclude('stubs')
	->in($dir = __DIR__.'/../src');

$versions = GitVersionCollection::create($dir)	
	->add('master', 'Master');

return new Sami($iterator, array(
	'title' => 'Larvel Cashier - Gerencianet',
	'versions' => $versions,
	'build_dir' => __DIR__.'/build/%version%',
	'cache_dir' => __DIR__.'/cache/%version%',
	'default_opened_level' => 2,
	'remote_repository' => new BitBucketRemoteRepository('tonetlds/cashier-gerencianet', dirname($dir)),
));
